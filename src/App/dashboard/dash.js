import React, { Component } from 'react';
import { View, Text , StatusBar,StyleSheet, ScrollView,TouchableOpacity } from 'react-native';
import {Header,Icon} from 'native-base'
import strings from '../assets/strings'
import CatSlider from '../Category/CatSlider'
import Loader from '../assets/Loader'


const {color,green} = strings
export default class Dash extends Component {
  static navigationOptions = ({navigation})=> {
  
   return{
      headerLeft:  <Icon onPress={()=>navigation.toggleDrawer()} name="menu" style={{color:'white',margin:10}} />,
      title : 'FashionMart',
     
    }
  }
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount = () => {
   
  }
  

  render() {
    return (
      <View style={{flex:1}}>
        <StatusBar translucent={true} backgroundColor={color} barStyle="light-content"/>
       
        {/* <Header style={styles.header}>
             <View style={{flex:1,alignItems:'flex-start'}}>
              <Icon onPress={()=>this.props.navigation.toggleDrawer()} name="menu" style={{color:'white',margin:10}} />
            </View>


            <View style={{flex:3}}>
              <Text style={{color:'#fff',fontSize:20,marginTop:10,fontWeight:'bold'}}>FashionMart</Text>
            </View>

            <View style={{flex:2,alignItems:'flex-end'}}>
              <Icon name="ios-notifications" style={{color:'white',margin:10}} />
            </View>
        </Header> */}

        <ScrollView >
          <View style={{flex:1,justifyContent: 'center',}}>
            <CatSlider navigation={this.props.navigation} />
            
          </View>    
        </ScrollView>
          
      </View>
    );
  }
}


const styles = StyleSheet.create({
    header:{flexDirection:'row',backgroundColor: color,justifyContent: 'center',marginTop:24}
})
