import {createStackNavigator,createDrawerNavigator, DrawerItems, SafeAreaView ,} from 'react-navigation';
import dash from './dash'
import firebase from 'react-native-firebase'
import React, { Component } from 'react';
import { View, Text,Image , TouchableOpacity,StatusBar,Dimensions,StyleSheet,ScrollView } from 'react-native';
import {Spinner,Icon} from 'native-base'
import strings from '../assets/strings'
import Category from '../Category/Category'
import ProductList from '../Products/ProductList'
import SingleProduct from '../Products/SingleProduct'
import Cart from '../Checkout/Cart'
import Address from '../Checkout/Address'
import getdata,{setdata} from '../functions/async'
import WishList from '../Products/WishList'
import NewAddress from '../Checkout/NewAddress'

var db = firebase.firestore()
const {color,green} = strings




const {height,width} = Dimensions.get('window')
const CustomDrawerContentComponent = (props) => (
  
  <SafeAreaView style={styles.container} forceInset={{ top: 'always', horizontal: 'never' }}>
     <View style={styles.box}>
       <Text style={{color:'#fff',fontSize:24,textAlign:'center'}}>Welcome</Text>
       </View>
     <ScrollView >
       <DrawerItems {...props} />
     </ScrollView> 
  </SafeAreaView>
  
);

const styles = StyleSheet.create({
  box:{
    backgroundColor: color,
    height: 200,
    justifyContent: 'center',
  },
  container:{
    
  }
})

class CartIcon extends Component{
  constructor(props) {
    super(props)
  
    this.state = {
       
    };
  };
  componentDidMount = () => {
    getdata('uid').then(uid=>{
      if (uid) {
       
         this.unsubscribe = db.collection('carts').where('uid',"==",uid)
                            .onSnapshot(res=>{
                              const size = res.size
                              this.setState({size})
                            })
      }
    })
  }
  
  componentWillUnmount(){
   this.unsubscribe()
  }
  
  render() {
    const {navigation} = this.props
    const {size} = this.state
    return (
     <View style={{justifyContent: 'space-around',flexDirection:'row'}}>
                     <Icon onPress={()=>navigation.navigate('WishList')} name="heart" style={{color:'white',marginRight:15,marginTop:5,opacity:0.8}} />
                     <Icon onPress={()=>navigation.navigate('Cart')} name="md-cart" style={{color:'white',marginRight:15,marginTop:5}} />
                       <View style={{height:14,width:14,borderRadius: 7,borderBottomWidth: 1,borderColor: '#fff', backgroundColor: green,position:'absolute',top:0,right:17,justifyContent: 'center',}}>
                         <Text style={{textAlign:'center',color:'#fff',fontSize:9,fontWeight: '400',}}> {size} </Text>
                       </View>
       </View>
    )
  }
}


const Stack = createStackNavigator({
    
    // dash: dash,
    // Category : Category,
    // Products: ProductList,
    // SingleProduct: SingleProduct,
    // 
    Address: Address,
    WishList: WishList,
    Cart : Cart,
    // 
    NewAddress : NewAddress,
},
{
   navigationOptions:({navigation}) => ({
     
     headerRight: <CartIcon navigation={navigation} /> ,
        headerStyle:{
            backgroundColor: color,
             marginTop: 24,
        },
         headerTintColor: '#fff',
    })
}
)

const Drawer = createDrawerNavigator({
     Home: Stack,
    
  //Credits: CreditScreen
  }, {
  contentComponent: CustomDrawerContentComponent,
  drawerWidth: width * 0.75,
  contentOptions: {
    activeTintColor: color
  }
})




export default Drawer;
