import React, { Component } from 'react';
import { View, Text,ScrollView ,TouchableOpacity,Image,StyleSheet,StatusBar,Modal , TouchableHighlight,Alert } from 'react-native';
import {strings,Loader} from '../assets'
import firebase from 'react-native-firebase'
import Variants from '../Products/Variants'
import {Icon} from 'native-base'
import Snackbar from 'react-native-snackbar'


const db = firebase.firestore()
const {color,pink,green,grey} = strings

export default class CartCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
        visible:false,
        data : {}
    };
  }
  componentDidMount = () => {
    const data = this.props.data.data()
    const cartId = this.props.data.id
    this.setState({data,cartId})
  }
  
  change = (quantity,rate) =>{
     const {cartId} = this.state
     let total = quantity*rate
     if (cartId) {
       db.collection('carts').doc(cartId).update({quantity,total})
       .then(res=>{})
       .catch(err=>console.log(err))
     }
  }

  remove =() =>{
    const cartId = this.state.cartId
    Alert.alert( 
      'Remove From Cart',
      'Are You Sure you want to remove this item?',
      [
        {text:'Cancel',onPress:()=>console.log('cancel')},
        {text:'Remove',onPress:()=>{
           db.collection('carts').doc(cartId).delete()
           .then(res=>{
             Snackbar.show({
               title: 'Removed Item From Cart',
               duration: Snackbar.LENGTH_SHORT
             })
           }).catch(err=>console.log(err))
        }},
    ]
    )
  }

   

  render() {
      const data = this.props.data.data()
      const visible = this.state.visible
      const quantity = data.quantity
      const rate = data.rate
    return (
      <View  style={{margin:5,backgroundColor: '#fff',padding:5,marginHorizontal: 8,borderRadius:4,borderWidth: 1,borderColor:grey}}>
                      <View style={{flex:1,flexDirection:'row'}}>
                         <Image style={{width:100,height:150,alignItems: 'stretch',margin:10}} source={{uri : data.img}}/>
                         <View style={{flex:1,margin:10}}>
                           <Text style={{fontSize:14,fontWeight: '500',}}> {data.name} </Text>
                           <Text style={{fontSize:12,fontWeight: '400',}}> {data.category} -> {data.subcat} </Text>
                           <Text style={{fontSize:16,fontWeight: '500',color:'#000',margin:5}}> ₹ {data.total} </Text>
                            
                            <View style={{height:50,flexDirection:'row',marginTop: 10,justifyContent: 'space-around',}}>
                               <Text style={{flex:2,fontSize:14,fontWeight: 'bold',alignSelf: 'center',}}>Quantity</Text>
                                  <View style={{flexDirection:'row',flex:3,alignItems: 'center'}}>

                                    <TouchableOpacity  activeOpacity={0.8} onPress={()=>this.change(quantity-1 || 1,rate)}
                                    style={{borderTopLeftRadius: 15,borderBottomLeftRadius: 15,width:40,elevation:4, height:30,justifyContent: 'center',backgroundColor: pink,}}>
                                          <Icon name="minus" type="Feather" style={{textAlign:'center',fontSize:20,color:'#fff'}}/>             
                                    </TouchableOpacity>
                                    <View style={{padding:8,backgroundColor: grey,justifyContent: 'center',height:30}}>
                                      <Text style={{fontSize:18,fontWeight: '500',color:pink,textAlign:'center'}}> {quantity } </Text>
                                    </View>

                                    <TouchableOpacity activeOpacity={0.8} onPress={()=>this.change(quantity+1, rate )}
                                    style={{borderTopRightRadius: 15,borderBottomRightRadius: 15,width:40,elevation:4, height:30,justifyContent: 'center',backgroundColor: pink,}}>
                                    <Icon name="md-add" style={{textAlign:'center',fontSize:20,color:'#fff'}}/>
                        
                                    </TouchableOpacity>
                                  </View>
                            </View>
                             {data.variant ?
                              <View style={{flexDirection:'row',flexWrap:'wrap',marginTop:10}}>
                                {Object.keys(data.variant).map((name,id)=>
                                    <View key={id} style={{padding:5,flexDirection:'row'}}>
                                      <Text style={{fontSize:12,fontWeight: '500',}}> {name} :</Text>
                                      <Text style={{fontSize:12,fontWeight: '500', color:green}}> {data.variant[name]} </Text>
                                    </View>
                                  )}
                              </View>
                              :null}
                         </View>
                      </View>
                      <View >
                         
                      </View>
                      <View style={{height:40,padding:3,borderTopWidth: 1,borderColor:grey,flexDirection:'row',marginTop: 10,}}>
                        <TouchableOpacity onPress={this.remove} activeOpacity={0.8} style={{flex:1,justifyContent: 'center',}}>
                          <Text style={{textAlign:'center',fontWeight: 'bold',fontSize:14}}>REMOVE</Text>
                        </TouchableOpacity>
                         
                         <View style={{height:34,width:1,backgroundColor: grey,}}/>
                      
                          <TouchableOpacity onPress={()=>this.setState({visible:true})}
                          activeOpacity={0.8} style={{flex:1,justifyContent: 'center',}}>
                            <Text style={{textAlign:'center',fontWeight: 'bold',fontSize:14,color:green}}>MODIFY</Text>
                        </TouchableOpacity>
                      </View>

                    
       </View>
                 
    );
  }
}
