import React, { Component } from 'react';
import { View, Text,ScrollView ,TouchableOpacity,Image,StyleSheet,StatusBar,Modal , TouchableHighlight,Alert,TextInput } from 'react-native';
import {strings,Loader} from '../assets'
import firebase from 'react-native-firebase'
import Variants from '../Products/Variants'
import {Icon,Spinner} from 'native-base'
import Snackbar from 'react-native-snackbar'
import axios from 'axios'

const db = firebase.firestore()
const {color,pink,green,grey} = strings

export default class CouponCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
       coupon:''
    };
  }
 componentWillUpdate = (nextProps, nextState) => {
   if (this.props.total != nextProps.total) {
       const {data,active} = this.state
      if (data && active) {
          this.props.getDiscount(data)
      }
   }
 }
 
  
  
  check = () =>{
     const coupon = this.state.coupon
     const total = this.props.total
     //alert(coupon)
     if (coupon.length == 6) {
       this.setState({checking:true})
       //do the work
           db.collection('coupons')
           .where('code',"==",coupon).limit(1).get()
           .then(res=>{
               
               const data = res.size ? res.docs[0].data() : null
               this.setState({checking:false})
               if (data) {
                   if (data.active) {
                       this.setState({data,active:data.active})
                       this.props.getDiscount(data)
                   } else {
                     Snackbar.show({
                       title: 'This Coupon Code Has Expired',
                       duration: Snackbar.LENGTH_SHORT
                     })
                   }
               }else{
                   Snackbar.show({
                     title: 'This Coupon Code Is not Valid',
                     duration: Snackbar.LENGTH_SHORT
                   })
               }
           })
        
     }else{
       Snackbar.show({
         title: 'Enter A Valid Coupon Code',
         duration: Snackbar.LENGTH_SHORT
       })
     }
  }
 
  render() {
      const checking = this.state.checking
      const {msg} = this.props
    return (
      <View  style={{margin:5,backgroundColor: '#fff',padding:5,marginHorizontal: 8,borderRadius:4,borderWidth: 1,borderColor:grey}}>
    
        <Text style={{margin:8,fontWeight: '500',}}>OFFER</Text>
                      <View style={{flex:1,flexDirection:'row',margin:8,borderWidth:1,borderColor: grey,height:50,justifyContent: 'space-between',}}>
                        <TextInput onChangeText={coupon => this.setState({coupon})} autoCapitalize="characters"
                        placeholder="ENTER COUPON CODE" underlineColorAndroid="transparent" style={{fontSize:14,marginHorizontal:8}}
                        maxLength={6}/>
                        
                        <TouchableOpacity  activeOpacity={0.7} onPress={this.check}
                            style={{height:35,width:120,justifyContent: 'center',backgroundColor: color,alignSelf: 'center',marginRight:5}}>
                           {checking ?
                             <Spinner color="#fff" style={{textAlign:'center'}}/>:
                              <Text style={{fontSize:16,fontWeight: '500',textAlign:'center',color:'#fff'}}> APPLY</Text>
                        }
                        </TouchableOpacity>
                      </View>

                    {msg ?
                    <Text style={{color:green,fontWeight: '500',textAlign:'center',margin:5}}> {msg} </Text>
                    :null} 
       </View>
                 
    );
  }
}
