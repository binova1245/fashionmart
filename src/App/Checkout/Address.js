import React, { Component } from 'react';
import { View, Text,StatusBar,ScrollView,TouchableOpacity } from 'react-native';
import {strings,Loader} from '../assets'
import firebase from 'react-native-firebase'
import getdata from '../functions/async';
import {Icon} from 'native-base'

const db = firebase.firestore()
const {color,pink,grey,green} = strings
export default class Address extends Component {
 static navigationOptions = {
     title : 'ADDRESS',
     headerRight : null
 }
  constructor(props) {
    super(props);
    this.state = {
    };
  }
  getAddress = (uid) =>{
      this.unsubscribe = db.collection('address').doc(uid)
                         .onSnapshot(res =>{
                             const list = res.exists ? res.data().list : null
                             this.setState({list,fetched:true})
                         })
  }

  componentDidMount = () => {
    getdata('uid').then(uid=>{
        this.setState({uid})
        this.getAddress(uid)
    }).catch(err=>console.log(err))
  };

  componentWillUnmount = () => {
    this.unsubscribe()
  };
  
  

  render() {
      const {list,fetched} = this.state
    return (
      <View style={{flex:1}}>
        <StatusBar translucent={true} backgroundColor={color} barStyle="light-content"/>
        {fetched ?
             list?
              <View style={{flex:1}}>
              <ScrollView sytle={{flex:1}}>
                {list.map((data,id)=>
                  <Text> {data.pincode} </Text>
                  )}
              </ScrollView>
              <TouchableOpacity activeOpacity={0.8} onPress={()=>{this.props.navigation.navigate('NewAddress',{list})}}
               style={{height:45,backgroundColor: color,justifyContent: 'center',}}>
                <Text style={{fontSize:16,color:"#fff",textAlign:'center'}}> ADD </Text>
              </TouchableOpacity>
              </View>
             :
             <View style={{flex:1,justifyContent: 'center',}}>
                <Icon name="local-shipping" type="MaterialIcons" style={{textAlign:'center',color:color,fontSize:100}}/>
                <Text style={{fontSize:18,textAlign:'center',margin:10}}>
                  No Delivery Address Is Added!
                </Text>
                <TouchableOpacity activeOpacity={0.7} onPress={()=>{this.props.navigation.navigate('NewAddress',{list})}}
                 style={{paddingVertical: 8, backgroundColor: green,justifyContent: 'center',borderRadius: 3,width:150,alignSelf: 'center',}}>
                  <Text style={{color:'#fff',textAlign:'center',fontSize:18}}>ADD NEW</Text>
                </TouchableOpacity>
             </View>
            : <Loader/>}
      </View>
    );
  }
}
