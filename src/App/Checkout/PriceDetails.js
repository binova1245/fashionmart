import React, { Component } from 'react';
import { View, Text,ScrollView ,TouchableOpacity,Image,StyleSheet,StatusBar,Modal , TouchableHighlight,Alert,TextInput } from 'react-native';
import {strings,Loader} from '../assets'
import firebase from 'react-native-firebase'
import Variants from '../Products/Variants'
import {Icon,Spinner} from 'native-base'
import Snackbar from 'react-native-snackbar'
import axios from 'axios'

const db = firebase.firestore()
const {color,pink,green,grey} = strings

export default class CartCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
       pincode:''
    };
  }
  componentDidMount = () => {
    
  }
  
  
 
  render() {
     const {total,weight,shipping,pincode,discount} = this.props.data
     const price = [
                   {name:'Total Price',value:total,alt:0,sign:''},
                   {name:'Discount',value:discount,color:green,sign:'-',alt:'Not Applied'},
                   {name:'Delivery Charges',value:shipping,alt:'Enter Pincode',sign:''},
                  ]
    return (
      <View  style={{margin:5,backgroundColor: '#fff',padding:5,marginHorizontal: 8,borderRadius:4,borderWidth: 1,borderColor:grey}}>
           {price.map((data,id)=>
             <View key={id} style={{padding:5,flexDirection:'row',justifyContent: 'space-between',}}>
               <Text>{data.name} </Text>
               <Text style={{color:data.color ? data.color : null}}>{data.value ? data.sign  + ' ₹ ' +data.value : data.alt  } </Text>
            </View> 
            )} 
            <View style={{height:1,backgroundColor: grey,}}   /> 
             <View style={{padding:5,flexDirection:'row',justifyContent: 'space-between',}}>
               <Text style={{color:'#000',fontWeight: '500',}}> TOTAL</Text>
               <Text style={{color:'#000',fontWeight: '500',}}>  ₹ {this.props.subTotal} </Text>
            </View>      
      </View>

      
                 
    );
  }
}
