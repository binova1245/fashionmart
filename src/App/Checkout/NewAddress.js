import React, { Component } from 'react';
import { View, Text,StatusBar,ScrollView,TouchableOpacity } from 'react-native';
import {strings,Loader} from '../assets'
import {Icon} from 'native-base'
import AdrPin from './AdrPin'
import { Jiro } from 'react-native-textinput-effects';
import getdata from '../functions/async'
import firebase from 'react-native-firebase'
import Snackbar from 'react-native-snackbar'

const db = firebase.firestore()
const {color,pink,green,grey} = strings
export default class NewAddress extends Component {
    static navigationOptions = {
      title: 'NEW ADDRESS',
      headerRight: null
    }
  constructor(props) {
    super(props);
    this.state = {
        domestic : true,
        phone:''
    };
  }

  getPin = (State, District, Circle,pincode) =>{
    this.setState({State,District,Circle,pincode,ifPin:true})
  }

  submit = () =>{
    const list = this.props.navigation.getParam('list',[])
    const {defaultAddress,State,District,Circle,ifPin,domestic,name,phone,address,uid,pincode} = this.state
    const pLength = phone.length == 10 ? true : false
    if (ifPin && name && address && phone && uid && pLength ) {
       this.setState({loading:true})
      const index =list.length 
      const obj = {pincode,State,District,Circle,name,phone,address,domestic}
      list.push(obj)
      defaultAddress ? list.reverse() : null
       db.collection('address').doc(uid).set({list})
       .then(res=>{
         this.setState({loading:false})
         Snackbar.show({
           title:'Address Added',
           duration: Snackbar.LENGTH_SHORT
         })
         this.props.navigation.navigate('Cart',{obj,pincode})
       }).catch(err=>{
         this.setState({loading:false})
          Snackbar.show({
            title: 'Unable To Save, Try Again!',
            duration: Snackbar.LENGTH_SHORT
          })
       })
        
    }else{
      const msg = pincode ? name ? address ?phone ? pLength? 'Something Went Wrong, Reopen the App!':
       'Please Enter A Valid Phone Number' : 'Please Enter Your Phone Number' : 'Please Enter An Address' : 'Please Enter Your Name' : 'Please Enter A Pincode'
      Snackbar.show({
        title:`${msg}`,
        duration:Snackbar.LENGTH_SHORT
      })
    }
  }

  componentDidMount = () => {
    getdata('uid').then(uid=>{
      this.setState({uid})
    
    }).catch(err=>console.log(err))
  }
  


  render() {
      const type = ["DOMESTIC", "INTERNATIONAL"]
      const {domestic,defaultAddress} = this.state
    return (
      <View style={{flex:1}}>
           <StatusBar translucent={true} backgroundColor={color} barStyle="light-content"/>
       
       <ScrollView >
             <Card >
                 <Text style={{margin:5,fontWeight: '500',}}>SHIPPING TYPE</Text>
            
                 <View style={{flex:1,flexDirection:"row",justifyContent: 'space-around',paddingVertical:10}}>
                   {type.map((data,id)=>
                     <View key={id} style={{flexDirection:"row",justifyContent: 'center',flex:1}}>
                       <Icon onPress={()=>this.setState({domestic:!id})}
                       name={domestic == id ? "md-radio-button-off" : "md-radio-button-on"} style={{color:color,alignSelf: 'center',}}/>
                       <Text style={{marginTop: 5,}}> {data} </Text>
                     </View>
                    )}
                 </View>
             </Card>

            
                 <View style={{flex:1,paddingVertical: 10,}}>
                  <AdrPin getPin={this.getPin} />
                 </View>
           
              <Card>
                  <Jiro
                  label={"Name"} onChangeText={name=>this.setState({name})}
                  // this is used as active and passive border color
                  borderColor={grey} style={{marginHorizontal:5}}
                  inputStyle={{ color: green }}
                />
                 <Jiro
                  label={"Address"} onChangeText={address=>this.setState({address})}
                  // this is used as active and passive border color
                  borderColor={grey} style={{marginHorizontal:5}}
                  inputStyle={{ color: green,fontSize:14,fontWeight: '400', }}
                />
                
                 <Jiro
                  label={"Phone Number"} onChangeText={phone=>this.setState({phone})}
                  // this is used as active and passive border color
                  borderColor={grey} style={{marginHorizontal:5}} keyboardType="numeric"
                  inputStyle={{ color: green }}
                />
                <View style={{height:40}}/>
                  <View style={{flexDirection:"row",justifyContent: 'flex-start',height:40,marginLeft:10}}>
                       <Icon onPress={()=>this.setState({defaultAddress:!defaultAddress})}
                       name={!defaultAddress ? "md-radio-button-off" : "md-radio-button-on"} style={{color:color,alignSelf: 'center',}}/>
                       <Text style={{marginTop: 10,}}> Make It Default Address </Text>
                     </View>
              </Card>
       </ScrollView>
        <TouchableOpacity activeOpacity={0.8} onPress={this.submit} style={{height:45,backgroundColor: color,justifyContent: 'center',}}>
          <Text style={{fontSize:16,color:"#fff",textAlign:'center'}}> ADD </Text>
        </TouchableOpacity>
      </View>
    );
  }
}


const Card = props =>{
    return(
        <View style={{margin:8,borderRadius:4,borderColor: grey,backgroundColor: '#fff',borderWidth:1, padding: 5,}}>
          {props.children}
        </View>
    )
}
