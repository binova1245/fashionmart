import React, { Component } from 'react';
import { View, Text,ScrollView ,TouchableOpacity,Image,StyleSheet,StatusBar,Modal , TouchableHighlight,Alert,TextInput } from 'react-native';
import {strings,Loader} from '../assets'
import firebase from 'react-native-firebase'
import Variants from '../Products/Variants'
import {Icon,Spinner} from 'native-base'
import Snackbar from 'react-native-snackbar'
import axios from 'axios'

const db = firebase.firestore()
const {color,pink,green,grey} = strings

export default class PinCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
       pincode:''
    };
  }
  componentWillUpdate = (nextProps, nextState) => {
   const {data,pincode,state} = this.state
   if(data && state){
     this.props.getShipping(data,state,pincode)
   }
  }
  
  check = () =>{
     const pincode = this.state.pincode
     //alert(pincode)
     if (pincode.length == 6) {
       this.setState({checking:true})
       //do the work
       const url = "http://postalpincode.in/api/pincode/"+pincode
       axios.get(url)
       .then(result=>{
           const state = result.data.Status == "Success" ? result.data.PostOffice[0].State : null
           if (state) {
              db.collection('charges')
              .where('state','==',state)
              .limit(1).get()
              .then(res =>{
                this.setState({checking:false})
                const data = res.size ? res.docs[0].data() : null
                this.setState({data,state})
                this.props.getShipping(data,state,pincode)
              })
         
           }else{
               this.setState({checking:false})
              Snackbar.show({
                title: 'Enter A Valid Pincode',
                duration: Snackbar.LENGTH_SHORT
              })
           }
          
       }).catch(err=>{
          this.setState({checking:false})
           Snackbar.show({
             title: 'Something went wrong, Try Again',
             duration: Snackbar.LENGTH_LONG,
               action: {
                 title: 'Try Again',
                 color: green,
                 onPress: () => { this.check() },
               },
           })
       })
     }else{
       Snackbar.show({
         title: 'Enter A Valid Pincode',
         duration: Snackbar.LENGTH_SHORT
       })
     }
  }
 
  render() {
      const checking = this.state.checking
    return (
      <View  style={{margin:5,backgroundColor: '#fff',padding:5,marginHorizontal: 8,borderRadius:4,borderWidth: 1,borderColor:grey}}>
    
        <Text style={{margin:8,fontWeight: '500',}}>CHECK PINCODE</Text>
                      <View style={{flex:1,flexDirection:'row',margin:8,borderWidth:1,borderColor: grey,height:50,justifyContent: 'space-between',}}>
                        <TextInput onChangeText={pincode => this.setState({pincode})} keyboardType="numeric"
                        placeholder="ENTER PINCODE" underlineColorAndroid="transparent" style={{fontSize:14,marginHorizontal:8}}
                        maxLength={6}/>
                        
                        <TouchableOpacity  activeOpacity={0.7} onPress={this.check}
                            style={{height:35,width:120,justifyContent: 'center',backgroundColor: green,alignSelf: 'center',marginRight:5}}>
                           {checking ?
                             <Spinner color="#fff" style={{textAlign:'center'}}/>:
                              <Text style={{fontSize:16,fontWeight: '500',textAlign:'center',color:'#fff'}}> CHECK</Text>
                        }
                        </TouchableOpacity>
                      </View>

                     
       </View>
                 
    );
  }
}
