import React, { Component } from 'react';
import { View, Text,ScrollView ,TouchableOpacity,Image,StyleSheet,StatusBar } from 'react-native';
import {strings,Loader} from '../assets'
import {Icon} from 'native-base'
import firebase from 'react-native-firebase'
import CartCard from './CartCard'
import PinCard from './PinCard'
import getdata from '../functions/async'
import PriceDetails from './PriceDetails'
import CouponCard from './CouponCard'

const db = firebase.firestore()
const {color,pink,green,grey} = strings
export default class Cart extends Component {
  static navigationOptions = {title:'Cart'}
  constructor(props) {
    super(props);
    this.state = {
      total: 0
    };
  }
  getCart = (uid) =>{
    db.collection('carts')
    .where("uid", "==", uid)//******************Change this userid */
    .onSnapshot(res=>{
      const docs = res.size ? res.docs : null
      this.setState({docs,fetched:true,})
      let total = 0
      let weight = 0
      docs.map((data,id)=>{
        total += data.data().total
        weight  += data.data().weight || 450
        if (id+1 == docs.length) {
          this.setState({total,weight})
        }
      })
     // alert(res.size)
    })
  }
  //get the shipping pincode and rate list
  getShipping = (data,state,pincode) =>{
    const rates = data ? data.rates : null
    const charge = data ? null : 50
    const {weight} = this.state
     const shipping = parseInt(weight*charge/1000)
     this.setState({state,pincode,shipping})
  }
  
  getDiscount = (data) =>{
    const {total} = this.state
    const {flat,discount,minimum,code} = data
    if (flat) {
      total+1 >= minimum ? 
      this.setState({discount,code, couponMsg: `A discount of ₹ ${discount} has been applied on the Total`})  :
      this.setState({couponMsg: `Minimum amount must be ₹ ${minimum} in order to use ${code}`}) 
    }else{
      const dis = parseInt(discount*total/100)
      this.setState({
            discount:dis,code,
            couponMsg: `A discount of ₹ ${dis} has been applied on the Total`})
    }
  }
  
  submit = () =>{
    //  const 
  }

  componentDidMount = () => {
    getdata('uid').then(uid=>{
      this.getCart(uid)
    }).catch(err=>{
      console.log(err)
    })
  
  }
  
   
  render() {
    const st = this.state
    const discount = st.discount || 0
    const shipping = st.shipping || 0
    const subTotal = st.total + shipping - discount
    return (
      <View style={{flex:1}} >
        <StatusBar translucent={true} backgroundColor={color} barStyle="light-content"/>
            {st.fetched ? st.docs ?
               <View style={{flex:1}}>
                  <ScrollView style={{flex:1}}>
                    <View style={{flexDirection:'row',justifyContent: 'space-between',padding:8}}>
                      <Text style={{fontWeight: '500',fontSize:14}}> ITEMS({st.docs.length}) </Text> 
                      <Text style={{fontWeight: '500',fontSize:14}}> TOTAL: ₹ {this.state.total} </Text> 
                    </View>
                    { st.docs.map((data,id)=>
                      <CartCard navigation={this.props.navigation} data={data} key={id} index={id} id={data.id}/>
                      )}
                      <PinCard getShipping={this.getShipping} />
                      
                      <CouponCard total={st.total} getDiscount={this.getDiscount} msg={st.couponMsg} />

                       <Text style={{fontWeight: '500',fontSize:14,margin:5}}> PRICE DETAILS </Text> 
                       <PriceDetails data={this.state} subTotal={subTotal} />
                  </ScrollView>
                   <View style={{height:60,backgroundColor: '#fff',justifyContent: 'space-around',elevation:8,flexDirection:'row',
                  borderTopWidth: 1,borderColor:grey, alignItems: 'center'}}>
                   <Text style={{fontWeight: '500',fontSize:18,color:'#000'}}> ₹ {subTotal} </Text> 
                  
                   <TouchableOpacity  activeOpacity={0.7}
                    style={{height:40,width:200,borderRadius:4,justifyContent: 'center',backgroundColor: strings.pink,}}>
                     <Text style={{fontSize:17,fontWeight: '500',textAlign:'center',color:'#fff'}}> PLACE ORDER </Text>
                  </TouchableOpacity>
                </View>
               </View> 
                 :
                 <View style={{flex:1,justifyContent: 'center',}}>
                     <Icon name="remove-shopping-cart" type="MaterialIcons" style={{textAlign:'center',fontSize:100,marginBottom: 20,}}/>
                   
                   <Text style={{textAlign:'center',fontSize:20,}}>
                     Nothing Added To The Cart!
                   </Text>
                 </View>
               : <Loader/>}
      </View>
    );
  }
}
