const strings = {
    // "color": "#3B3B98",
    "color": "#ff6b81",
    "pink": "#ff6b81",
    "grey": '#dfe4ea',
    "green": "#2ed573",
    "silver": "#dfe6e9",
    "title" :{"fontWeight":'bold','fontSize':18,'color':'#000'},
    "category": {"fontWeight":'400','fontSize':15},
    "price": [
        {
            "name" : "Piece",
            "number": 1,
        },
         {
           "name": "Bundle",
           
         }
    ],
    "size" : [
       
        {
            "name" : "Small",
            "sign" : "S"
        }, {
            "name" : "Medium",
            "sign" : "M"
        }, {
          "name": "Large",
          "sign": "L"
        }, {
          "name": "Extra Large",
          "sign": "XL"
        }, {
            "name" : "XX Large",
            "sign" : "XXL"
        }
    ]
}

export default strings; 