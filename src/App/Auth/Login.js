import React, { Component } from 'react';
import { View, Button, Text, TextInput, Image,StatusBar,TouchableOpacity,ToastAndroid } from 'react-native';
import CodeInput from 'react-native-confirmation-code-input';
import strings from '../assets/strings'

import firebase from 'react-native-firebase';

const successImageUri = 'https://cdn.pixabay.com/photo/2015/06/09/16/12/icon-803718_1280.png';
const color = strings.color

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.unsubscribe = null;
    this.state = {
      user: null,
      message: '',
      codeInput: '',
      phoneNumber: '',
      confirmResult: null,
    };
  }

  componentDidMount() {
    this.unsubscribe = firebase.auth().onAuthStateChanged((user) => {
      if (user) {
          this.props.navigation.navigate('App')
        //this.setState({ user: user.toJSON() });
      } else {
        // User has been signed out, reset the state
        this.setState({
          user: null,
          message: '',
          codeInput: '',
          phoneNumber: '',
          confirmResult: null,
        });
      }
    });
  }

  componentWillUnmount() {
     if (this.unsubscribe) this.unsubscribe();
  }

  signIn = () => {
    const { phoneNumber } = this.state;
    this.setState({ message: 'Sending OTP ...' });

    firebase.auth().signInWithPhoneNumber('+91'+phoneNumber)
      .then(confirmResult => this.setState({ confirmResult, message: 'Otp has been sent!' }))
      .catch(error => this.setState({ message: `Otp not sent!` }));
  };

  confirmCode = () => {
    const { codeInput, confirmResult } = this.state;

    if (confirmResult && codeInput.length) {
      confirmResult.confirm(codeInput)
        .then((user) => {
          this.setState({ message: 'OTP Confirmed!' });
        })
        .catch(error => this.setState({ message: `You Entered A Wrong OTP` }));
    }
  };

  signOut = () => {
    firebase.auth().signOut();
  }

  renderPhoneNumberInput() {
   const { phoneNumber } = this.state

    return (
      <View style={{ padding: 25 }}>
        <Text style={{textAlign:'center'}}>Login Using Your Phone Number</Text>

        <View style={{flexDirection:'row',justifyContent: 'center',alignItems:'center'}}>
          <Text style={{fontSize:22,color:color,fontWeight:'500'}}>+91</Text>

           <TextInput
                autoFocus
                keyboardType="number-pad"
                maxLength={10}
                style={{ height: 60, marginTop: 15, marginBottom: 15,fontSize:20 }}
                onChangeText={value => this.setState({ phoneNumber: value })}
                placeholder={'Phone number..'}
                value={phoneNumber}
            />
        </View>
       
        <Button title="Sign In" color={color} onPress={this.signIn} />
      </View>
    );
  }

  renderMessage() {
    const { message } = this.state;

    if(message.length){
       // ToastAndroid.showWithGravity(
       //     `${message}`,
       //     ToastAndroid.SHORT,
        //    ToastAndroid.BOTTOM,
        //  );
        //alert(message)
    }
  
  }

  renderVerificationCodeInput() {
    const { codeInput } = this.state;

    return (
      <View style={{ marginTop: 25, padding: 25,flex:1 }}>
       

        <Text>Enter verification code below:</Text>
        <CodeInput
            ref="codeInputRef2"
            secureTextEntry
            codeLength={6}
            activeColor='rgba(49, 180, 4, 1)'
            inactiveColor='rgba(49, 180, 4, 1.3)'
            autoFocus={false}
            ignoreCase={true}
            inputPosition='center'
            size={40}
            onFulfill={(code) => this.setState({codeInput:code})}
            containerStyle={{ marginTop: 20 }}
            codeInputStyle={{ borderWidth: 1.5 }}
        />
        <Button title="Verify Code" color={color} onPress={this.confirmCode} />
      </View>
    );
  }

  render() {
    const { user, confirmResult } = this.state;
    return (
      <View style={{ flex: 1 }}>
       <StatusBar translucent={false} backgroundColor={color}/>
        
        <View style={{height:200,backgroundColor: color,justifyContent: 'center',alignItems:'center',elevation:6}}>
           <Text style={{color:'#fff',fontSize:30,fontWeight:'500'}}>FashionMart</Text>
           <Text style={{color:'#eee',fontSize:16,textAlign:'right'}}>Admin App</Text>
        </View>

        {!user && !confirmResult && this.renderPhoneNumberInput()}

        {this.renderMessage()}

        {!user && confirmResult && this.renderVerificationCodeInput()}

        
      </View>
    );
  }
}
