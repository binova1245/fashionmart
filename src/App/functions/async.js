import {AsyncStorage} from 'react-native'
 

  getdata = async (key)=>{
    try{
      var item = await AsyncStorage.getItem(key);
      return item;
    } catch(err){
      console.log('error',err)
    }
  }

    const setdata = async (key,item) =>{
  	try{
        
        await AsyncStorage.setItem(key,item);
           
  	} catch(error){
         console.log(error.message);
  	}
  }

  export default getdata;
  export {setdata} 
