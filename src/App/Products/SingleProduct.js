import React, { Component } from 'react';
import { View, Text ,StyleSheet ,Image,Dimensions,ScrollView,TouchableOpacity,StatusBar,Modal} from 'react-native';
import {strings,Loader} from '../assets'
import firebase from 'react-native-firebase'
import Carousel from 'react-native-snap-carousel';
import {Icon} from 'native-base'
import Variants from './Variants'
import Snackbar from 'react-native-snackbar'
import CartModal from './CartModal';
import getdata from '../functions/async'
import SlidingUpPanel from 'rn-sliding-up-panel';



const db = firebase.firestore()
const {color,pink,grey,green} = strings
let {height,width} = Dimensions.get('screen')
export default class SingleProduct extends Component {
  static navigationOptions = ({navigation}) =>{
    return{
      title : `${navigation.getParam('name','')}`
    }
  }
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  getProduct = (pid) =>{
      db.collection('products').doc(pid)
      .onSnapshot(res=>{
          const doc = res.exists ? res : null
          this.setState({doc,id : res.id ,fetched : true})
      })
  }

  componentDidMount = () => {
     const doc = this.props.navigation.getParam('data',null)
     const pid = this.props.navigation.getParam('pid', '1546752467477')
     pid ? this.getProduct(pid) : this.setState({doc,fetched:true,pid : doc.id})
    // this.getProduct()
  
      getdata('uid').then(uid=>{
       this.setState({uid})
       this.unsubscribe = db.collection('wishlists').doc(uid)
                          .onSnapshot(res=>{
                            const list = res.exists ? res.data().list : []
                            const wishlist = list.includes(pid || doc.id)
                           // alert(wishlist)
                            this.setState({list,wishlist,pid : pid || doc.id})
                          })
     }).catch(err=>console.log(err))
  

    }
  
  wishList = () =>{
      const {list,uid,pid,wishlist} = this.state
      const bool = !wishlist
       bool ? list.push(pid) : list.splice(list.indexOf(pid),1)
        this.setState({wishlist:bool})
      db.collection('wishlists').doc(uid).set({list,uid})
      .then(res =>{
          const msg = bool ? ' Added To the WishList' : ' Removed From Wishlist'
          Snackbar.show({
            title: `${msg}`,
            duration: Snackbar.LENGTH_SHORT
          })
      }).catch(err=>console.log(err))
      
     //sync with the database here,

  }
  componentWillUnmount = () => {
    this.unsubscribe()
  }
  
  
  _renderItem =({item, index})=> {
     
        return (
            <View style={{height:height*0.75}}>
                <Image style={{flex:1}} source={{uri : item}}/>
                <Text style={{position:'absolute',left:5,bottom:5,color:'#fff',fontWeight: 'bold',fontSize:24}}>Shri</Text>
            </View>
        );
    }

    getVariant = obj =>{
      this.setState({variant:obj})
    }

  render() {
      const st = this.state
      const data = st.doc ? st.doc.data() : null
      const wishList = st.wishlist
      const modal = this.state.modal || false
    //   width=width*0.9
    return (
      <View style={{flex:1,}}>
                <StatusBar backgroundColor={color} translucent={true} barStyle="light-content"/>
         {st.fetched ?
              <ScrollView style={{flex:1}}>
                 <Carousel
                //   layout={'stack'} layoutCardOffset={`9`}
                    ref={(c) => { this._carousel = c; }}
                    data={data.img}
                    renderItem={this._renderItem}
                    sliderWidth={width}
                    itemWidth={width}
                  />
                  <View style={{flex:1}}>
                  <View style={{padding:8,backgroundColor: '#fff',}}>
                     <Text style={strings.title}> {data.name} </Text>
                     <Text style={strings.category}> {data.category} -> {data.subcat} </Text>
                     {data.Piece.price ?
                       <View style={styles.priceBox}>
                        <Text style={styles.price}>₹ {data.Piece.price || 'N/A'} </Text>
                        <Text style={{fontSize:14,marginTop:5, fontWeight: '500',color:strings.pink}}> (Per Peice) </Text>
                     </View>:null}
                      {data.Bundle.price? //if Bundle price is active then only show 
                        <View style={styles.priceBox}>
                        <Text style={[styles.price,{color:strings.pink,elevation:6,fontSize:20}]}>₹ {data.Bundle.price || 'N/A'} </Text>
                        <Text style={{fontSize:14,marginTop:5, fontWeight: '500',color:strings.pink}}> (Bundle Price) </Text>
                        <Text style={{fontSize:14,marginTop:5, fontWeight: '500',color:strings.green,textAlign:'right',flex:1}}>
                         {data.Bundle.number ? data.Bundle.number + ' Pieces in a Bundle ' : null} </Text>
                     </View>:null}
                     <Text style={{padding:5,fontSize:12,fontWeight: 'bold',}}>* Shipping Charges Are Applicable</Text>
                  
                   </View>  
                    

                    {data.variant?
                      <Variants data={data.variant} getVariant={this.getVariant} />
                     :null}

                      <View style={{flex:1,padding:8,backgroundColor: '#fff',marginTop: 10,}}>
                        <Text style={[strings.title,{fontSize:17}]}>Description :</Text>
                        <Text style={{fontSize:12,margin:10,fontWeight: 'bold',color:'#000'}}>{data.details}</Text>
                     </View>
                   </View>
              </ScrollView>
              :<Loader/>}

              {st.fetched ?
                <Icon name="heart" type="FontAwesome" style={{position:'absolute',top:8,right:8,color:wishList ? pink : grey,
                  opacity: wishList? 1 : 0.5 ,fontSize: 35,}}  />
             :null}

              {st.fetched ?
                <Icon name="heart-o" onPress={this.wishList} type="FontAwesome" style={{position:'absolute',top:8,right:8,color:pink,fontSize: 35,}}/>
             :null}

              {st.fetched ? 
                <View style={{height:60,backgroundColor: '#fff',justifyContent: 'space-around',elevation:!modal?8:0,flexDirection:'row',
                borderTopWidth: 1,borderColor:grey, alignItems: 'center'}}>
                  <TouchableOpacity activeOpacity={0.7} onPress={this.enquire}
                  style={{height:40,width:100,backgroundColor:strings.green,borderRadius:4,justifyContent: 'center',}}>
                     <Text style={{fontSize:20,fontWeight: 'bold',textAlign:'center',color:'#fff',}}>{st.enqId ? 'Enquired' : 'Enquire'}</Text>
                  </TouchableOpacity>

                   <TouchableOpacity  activeOpacity={0.7} onPress={() =>data.variant || data.Bundle.price ?  this.setState({modal:true}) : null }
                    style={{height:40,width:180,borderRadius:4,justifyContent: 'center',backgroundColor: strings.pink,}}>
                     <Text style={{fontSize:20,fontWeight: 'bold',textAlign:'center',color:'#fff'}}>{st.cartId ? 'Proceed' : 'Add'} To Cart</Text>
                  </TouchableOpacity>
                </View>
                :null}

              <SlidingUpPanel draggableRange={{top:height*0.75,bottom:50}} height={height*0.75} allowDragging={false}
               onRequestClose={()=>this.setState({modal:false})} visible={modal} transparent={true}>
                <CartModal data={data} pid={st.pid} uid={st.uid} goBack={this.addCart}  />
              </SlidingUpPanel>
               

      </View>
    );
  }

  addCart = (cartId) =>{
   this.setState({modal:false,cartId})
         
  }

  enquire = () =>{
    const {id,uid} = this.state
    alert('Your Response is recorded, Our Executives will get to you soon.')
    this.setState({enqId:123})
  }
}


const styles = StyleSheet.create({
    priceBox:{flexDirection:'row',paddingHorizontal:5},
    price:{fontSize:18,fontWeight: '500',color:'#000'}
})