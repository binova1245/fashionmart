import React, { Component } from 'react';
import { View, Text,ScrollView,TouchableOpacity } from 'react-native';
import {strings} from '../assets'

export default class Variants extends Component {
  constructor(props) {
    super(props);
    this.state = {
        variants:[]
    };
  }

  componentDidMount = () => {
    const variants = this.props.data
    this.setState({variants})
  }

click = (id,sid) =>{
    const variants = this.state.variants
    let subvar = variants[id].subvar[sid]
    let bool = !subvar.active
    variants[id].subvar[sid].active = bool
    this.setState({variants})
    //this.props.getVariant(variants)
}
  
  
  render() {
      const variants = this.state.variants
    return (
       <View style={{flex:1,padding:8,backgroundColor: '#fff',marginTop: 10,}}>
                         {variants.map((variant,id)=> variant.toggle ?
                           <View key={id} style={{flex:1}}>
                              <Text style={{fontSize:16,fontWeight: 'bold',color:'#000',margin:10}}>Select {variant.name} </Text>
                              {variant.subvar ? 
                                <View  style={{flex:1,paddingVertical: 8,flexDirection:'row',flexWrap: 'wrap',}}>
                                 {  variant.subvar.map((subvar,sid)=> subvar.toggle ?
                                     <TouchableOpacity activeOpacity={0.7} onPress={()=>{this.click(id,sid)}} key={id}
                                      style={{borderColor:strings.pink,borderWidth: 2,padding:3,elevation : subvar.active ? 6 : 0,borderRadius: 15,height:30,
                                      justifyContent: 'center',margin: 5,backgroundColor: subvar.active ? strings.pink : 'transparent',}}>
                                         <Text style={{fontWeight: 'bold',color:subvar.active ? '#fff' : strings.pink,textAlign:'center',fontSize:16}}> {subvar.name} </Text>
                                     </TouchableOpacity> : null
                                   )}
                                </View>
                                 :
                                 <Text style={{padding:5,fontSize:12,fontWeight: 'bold',}}>* No {variant.name} options are available </Text>
                   }
                           </View>:null
                          )}
         </View>
    );
  }
}
