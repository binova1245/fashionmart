import React, { Component } from 'react';
import { View, Text ,TouchableOpacity,Image,ScrollView,StyleSheet,StatusBar,Dimensions} from 'react-native';
import {strings,Loader} from '../assets'
import firebase from 'react-native-firebase'
import {Icon} from 'native-base'
import Snackbar from 'react-native-snackbar'


const {color,pink,green,grey} = strings
const {height,width} = Dimensions.get('screen')
const db = firebase.firestore()
export default class CartModal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      single : true,
      quantity:1,
      variant : {}
    };
  }

  click = (id,sid,varnt) =>{
     const variant = this.state.variant || {}
     const name = varnt.name
     const sname = varnt.subvar[sid].name
     variant[name] = sname
     this.setState({variant})
  }

  submit = (total,rate,variants) =>{
    const st = this.state
    const {variant,quantity} = this.state
    variants = variants 
    const varLength = Object.keys(variant).length
    //alert(variants.length)
    const ifVar = variants ? varLength == variants.length ? true : false : true // If there are no variations are availble in that case
    const type = st.single ? 'Piece' : 'Bundle'
    const {uid,pid,data} = this.props
    const img = data.img[0]
    const {name,category,subcat} = data
    console.log(ifVar,quantity,type,total,rate,uid,pid)
    if (ifVar && quantity && type && total && rate && uid && pid ) {
        this.setState({loading:true})
   
        const obj ={variant,quantity,type,total,rate,uid,pid,img,name,category,subcat}
         db.collection('carts').add(obj)
         .then(res=>{
            Snackbar.show({
              title: 'Added To Cart',
              duration: Snackbar.LENGTH_SHORT,
            });
             this.setState({loading:false})
   
            this.props.goBack(res.id)
         }).catch(err=>{
            this.setState({loading:false})
   
              Snackbar.show({
                title: 'Something Went wrong, Try Again!',
                duration: Snackbar.LENGTH_SHORT,
              });
         })
    }else{
      
     Snackbar.show({
                  title: `Please Select A Variant`,
                  duration: Snackbar.LENGTH_SHORT,
                });
     
    }
  
    
  }

  render() {
      const data = this.props.data || {}
       const variants = data.variant
       const stvar = this.state.variant || {}
       const single = this.state.single
       const quantity = this.state.quantity || 1
       const rate = single ? data.Piece.price : data.Bundle.price
       const total = rate*quantity
    return (
      <View style={{height:height*0.5, zIndex:6, backgroundColor: '#fff',elevation:6,}}>
       
        {this.state.loading ? <Loader/> :null}
         <View style={{height:50,backgroundColor: color,justifyContent: 'flex-start', padding:8,flexDirection:'row',elevation:6}}>
            <Icon name="md-close" onPress={()=>this.props.goBack()} style={{fontSize:30,color:'#fff',alignItems: 'center',marginLeft:8}}/>
            <Text style={{fontSize:20,fontWeight: 'bold',color:'#fff',alignItems: 'center',marginLeft: 20,}}> {data.name} </Text>
         </View>
          <ScrollView style={{flex:1}}>
               {data.Piece.price && data.Bundle.price ?
               <View style={{marginBottom:10}}>
                 <View style={{flexDirection:'row',justifyContent: 'space-around',borderColor:pink,borderBottomWidth: 1,}}>
                   <TouchableOpacity onPress={()=>this.setState({single:true})}
                    style={{flex:1,padding:8,backgroundColor: single ? pink : 'transparent',elevation:single ?6 :0}}>
                      <Text style={{textAlign:'center',color:single ? '#fff' : pink ,fontSize:20}}>Single</Text>
                    </TouchableOpacity>

                      <TouchableOpacity onPress={()=>this.setState({single:false})}
                       style={{flex:1,padding:8,backgroundColor: !single ? pink : 'transparent',elevation:!single ?6 :0}}>
                         <Text style={{textAlign:'center',color:!single ? '#fff' : pink ,fontSize:20}}>Bundle</Text>
                    </TouchableOpacity>  
                 </View>
               </View>
              :
              null
            }
              {variants ?
               <View style={{flex:1,padding:8,backgroundColor: '#fff',marginTop: 10,}}>
                         {variants.map((variant,id)=> variant.toggle ?
                           <View key={id} style={{flex:1}}>
                              <Text style={{fontSize:16,fontWeight: 'bold',color:'#000',margin:10}}>Select {variant.name} </Text>
                              {variant.subvar ? 
                                <View  style={{flex:1,paddingVertical: 8,flexDirection:'row',flexWrap: 'wrap',}}>
                                 {  variant.subvar.map((subvar,sid)=> 
                                     <TouchableOpacity activeOpacity={0.8} onPress={()=>{subvar.toggle ? this.click(id,sid,variant) : null}} key={id}
                                      style={{borderColor:strings.pink,borderWidth: 2,padding:5,elevation : subvar.name == stvar[variant.name] ? 6 : 0,borderRadius: 18,height:36,opacity:subvar.toggle ? 1:0.3,
                                      justifyContent: 'center',margin: 5,backgroundColor:  subvar.name == stvar[variant.name]  ? strings.pink : 'transparent',}}>
                                         <Text style={{fontWeight: 'bold',color: subvar.name == stvar[variant.name]  ? '#fff' : strings.pink,textAlign:'center'}}> {subvar.name} </Text>
                                     </TouchableOpacity> 
                                   )}
                                </View>
                                 :
                                 <Text style={{padding:5,fontSize:12,fontWeight: 'bold',}}>* No {variant.name} options are available </Text>
                   }
                           </View>:null
                          )}
         </View>
    
             : null}
            
            <View style={{flexDirection:'row',justifyContent: 'space-around',marginTop:30}}>
               <Text style={{flex:1,textAlign:'center',fontSize:20,color:pink,fontWeight: 'bold',alignItems: 'flex-end',}}>Quantity</Text>
               <View style={{flexDirection:'row',flex:1,alignItems: 'center',}}>
                <TouchableOpacity  activeOpacity={0.8} onPress={()=>this.setState({quantity:quantity-1 || 1})}
                 style={{borderTopLeftRadius: 20,borderBottomLeftRadius: 20,width:40, height:40,justifyContent: 'center',backgroundColor: pink,}}>
                       <Icon name="minus" type="Feather" style={{textAlign:'center',fontSize:25,color:'#fff'}}/>             
                </TouchableOpacity>
                <View style={{padding:8,backgroundColor: grey,justifyContent: 'center',height:40}}>
                  <Text style={{fontSize:18,fontWeight: '500',color:pink,textAlign:'center'}}> {quantity } </Text>
                </View>

                <TouchableOpacity activeOpacity={0.8} onPress={()=>this.setState({quantity:quantity+1 })}
                 style={{borderTopRightRadius: 20,borderBottomRightRadius: 20,width:40, height:40,justifyContent: 'center',backgroundColor: pink,}}>
                 <Icon name="md-add" style={{textAlign:'center',fontSize:25,color:'#fff'}}/>
    
                </TouchableOpacity>
               </View>
            </View>

          </ScrollView>
           {/* <View style={{justifyContent: 'center',margin:20}}>
              <Text style={{fontSize:30,fontWeight: '500',color:green,textAlign:'center'}}> 
               ₹ {total}
              </Text>
           </View> */}
          <TouchableOpacity activeOpacity={0.8} onPress={()=>this.submit(total,rate,variants)}
           style={{height:45,justifyContent: 'center',backgroundColor: pink,position:'absolute',bottom:30,left:0,width:width}}>
             <Text style={{textAlign:'center',color:'#fff',fontSize:20}}>DONE</Text>
          </TouchableOpacity>
      </View>
    );
  }
}
