import React, { Component } from 'react';
import { View, Text,ScrollView,StyleSheet,Image,TouchableOpacity,Dimensions,StatusBar } from 'react-native';
import firebase from 'react-native-firebase'
import {strings,Loader} from '../assets'
import {Icon} from 'native-base'
import getdata,{setdata} from '../functions/async'
import Snackbar from 'react-native-snackbar'

const {height,width} = Dimensions.get('screen')
const {color,grey,pink} = strings
const db = firebase.firestore()
export default class ProductList extends Component {
    static navigationOptions = ({navigation}) =>{
        return{
            title : `${navigation.getParam('name',null)}`
        }
    }
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  getProducts = (cat,subcat) =>{
      //alert(cat)
    db.collection('products')
    .where("category","==",cat)
    .where("subcat","==",subcat)
    .orderBy("uploadTime","DESC")
    .get().then(res=>{
          const docs = res.size ?  res.docs : null
          this.setState({docs,fetched:true})
    }).catch(err=>console.log(err))
  }

  componentDidMount = () => {
      const cat =  this.props.navigation.getParam('cat',null)
      const subcat =  this.props.navigation.getParam('name',null)
     cat && subcat ? this.getProducts(cat,subcat) : null

     //get the userid
     getdata('uid').then(uid=>{
       this.setState({uid})
       this.unsubscribe = db.collection('wishlists').doc(uid)
                          .onSnapshot(res=>{
                            const list = res.exists ? res.data().list : []
                          
                            this.setState({list})
                          })
     }).catch(err=>console.log(err))
  

    }
  
  wishList = (id,index) =>{
      const docs = this.state.docs
    
      const {list,uid} = this.state
      const bool = !list.includes(id)
       bool ? list.push(id) : list.splice(list.indexOf(id),1)
        this.setState({list})
      db.collection('wishlists').doc(uid).set({list,uid})
      .then(res =>{
          const msg = bool ? 'One Item Is Added To the WishList' : 'One Item Removed From Wishlist'
          Snackbar.show({
            title: `${msg}`,
            duration: Snackbar.LENGTH_SHORT
          })
      }).catch(err=>console.log(err))
      
     //sync with the database here,

  }
  componentWillUnmount = () => {
    this.unsubscribe()
  }
  

  render() {
      const st = this.state
      const list = this.state.list || []
    return (
      <View style={{flex:1,backgroundColor: st.fetched ? grey : null ,}}>
         <StatusBar translucent={true} backgroundColor={color}  barStyle="light-content"/>
        {st.fetched? st.docs ?
        <ScrollView style={{flex:1}}>
            <View style={{flexDirection:'row',flexWrap: 'wrap',justifyContent:'space-around',}}>
              { st.docs.map((data,id)=>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate('SingleProduct',{data,name:data.data().name})}
                 key={id} activeOpacity={0.8} style={{width:width*0.5 - 0.4,height:width*0.8,marginTop:1}}>
                   <Image source={{uri : data.data().img[0]}} style={{flex:3,alignSelf: 'stretch',}}/>
                   <View style={{flex:1,backgroundColor: '#fff',padding:3}}>
                     <Text style={{fontSize:13,fontWeight: 'bold',}}> {data.data().name} </Text>
                     <Text style={{fontSize:10,fontWeight: '400',}}> {data.data().category} </Text>
                     <Text style={{fontSize:16,fontWeight: 'bold',color:'#000'}}> ₹{data.data().Piece.price || N/A} </Text>
                   </View>
                   <Icon name="heart" type="FontAwesome" onPress={() => this.wishList(data.id,id)}
                   style={{fontSize:25,position:'absolute',right:8,bottom:5,color:list.includes(data.id)? strings.pink : grey,}}/>
                </TouchableOpacity>
               )}
            </View>
         </ScrollView> : null : <Loader/> }
      </View>
    );
  }
}
