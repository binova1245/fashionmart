import React, { Component } from 'react';
import { View, Text,ScrollView,StyleSheet,Image,TouchableOpacity,Dimensions,StatusBar } from 'react-native';
import firebase from 'react-native-firebase'
import {strings,Loader} from '../assets'
import {Icon} from 'native-base'
import getdata,{setdata} from '../functions/async'
import Snackbar from 'react-native-snackbar'

const {height,width} = Dimensions.get('screen')
const {color,grey,pink} = strings
const db = firebase.firestore()
export default class WishList extends Component {
    static navigationOptions = ({navigation}) =>{
        return{
            title : `WishList`
        }
    }
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  getProducts = (uid) =>{
      //alert(cat)
     this.unsubscribe = db.collection('wishlists')
    .doc(uid)
    .onSnapshot(res=>{
          const list = res.exists ?  res.data().list : []
          let docs =[]
          list.map((pid,id)=>{
          db.collection('products').doc(pid)
              .get().then(result =>{
                  const pdata = result.exists ? result : null
                  pdata.pid = pid
                  docs.push(pdata)
                  if (list.length == docs.length) {
                    //   alert(docs.length)
                     this.setState({docs,list,fetched:true}) 
                  }
              })
          })
    }).catch(err=>console.log(err))
  }
  
   wishList = (id) =>{
      
     Snackbar.show({
         title : 'Remove This Item From WishList',
         duration: Snackbar.LENGTH_LONG,
         action:{
             title : 'REMOVE',
             color: 'red',
             onPress : () =>{
                   const {list,uid} = this.state
                        const index = list.indexOf(id)
                        
                        list.splice(index,1)
                        
                            this.setState({list})
                        db.collection('wishlists').doc(uid).set({list,uid})
                        .then(res =>{
                            const msg =  'One Item Removed From Wishlist'
                            Snackbar.show({
                                title: `${msg}`,
                                duration: Snackbar.LENGTH_SHORT
                            })
                        }).catch(err=>console.log(err))
      
             }
         }
     })

  }

  componentDidMount = () => {
      
     //get the userid
     getdata('uid').then(uid=>{
       this.setState({uid})
       this.getProducts(uid)
     }).catch(err=>console.log(err))
  

    }

    componentWillUnmount = () => {
      // this.unsubscribe()
    };
    
  


  render() {
      const st = this.state
      const list = this.state.list || []
    return (
      <View style={{flex:1,backgroundColor: st.fetched ? grey : null ,}}>
         <StatusBar translucent={true} backgroundColor={color}  barStyle="light-content"/>
        {st.fetched? st.docs ?
        <ScrollView style={{flex:1}}>
            <View style={{flexDirection:'row',flexWrap: 'wrap',justifyContent:'space-around',}}>
              { st.docs.map((data,id)=>
                <TouchableOpacity onPress={()=>this.props.navigation.navigate('SingleProduct',{data,name:data.data().name})}
                 key={id} activeOpacity={0.8} style={{width:width*0.5 - 0.4,height:width*0.8,marginTop:1}}>
                   <Image source={{uri : data.data().img[0]}} style={{flex:3,alignSelf: 'stretch',}}/>
                   <View style={{flex:1,backgroundColor: '#fff',padding:3}}>
                     <Text style={{fontSize:13,fontWeight: 'bold',}}> {data.data().name} </Text>
                     <Text style={{fontSize:10,fontWeight: '400',}}> {data.data().category} </Text>
                     <Text style={{fontSize:16,fontWeight: 'bold',color:'#000'}}> ₹{data.data().Piece.price || N/A} </Text>
                   </View>
                   <Icon name="close" type="FontAwesome" onPress={() => this.wishList(data.pid)}
                   style={{fontSize:25,position:'absolute',right:8,bottom:8,color:"#ddd",elevation:6}}/>
                </TouchableOpacity>
               )}
            </View>
         </ScrollView> : null : <Loader/> }
      </View>
    );
  }
}
