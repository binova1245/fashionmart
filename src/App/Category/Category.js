import React, { Component } from 'react';
import { View, Text ,StyelSheet,StatusBar,Image,ScrollView,TouchableOpacity } from 'react-native';
import {strings,Loader} from '../assets'
import firebase from 'react-native-firebase'
import CatProductList from '../Products/CatProductList'

const color = strings.color
const db = firebase.firestore()
export default class Category extends Component {
    static navigationOptions = ({navigation}) =>{
        return{
            title: `${navigation.getParam('name','Category')}`
        }
    }
  constructor(props) {
    super(props);
    this.state = {
        docs:[]
    };
  }

  

  componentDidMount = () => {
    const data = this.props.navigation.getParam('data',null)
    this.setState({data,fetched:true})
  }
  

  render() {
     const data = this.state.data || {}
    return (
      <View style={{flex:1}}>
        <StatusBar translucent={true} backgroundColor={color} barStyle="light-content"/>
        {data?
         data.subcat ?
          <ScrollView style={{flex:1}}>
          { data.subcat.map((subcat, id) =>
               <View key={id} >
                 <TouchableOpacity activeOpacity={0.8} onPress={() => this.props.navigation.navigate('Products',{name:subcat.name,cat:data.name})}
                 style={{height:40,justifyContent: 'flex-end',backgroundColor: '#fff',marginBottom:1}}>
                  <Text style={{fontSize:14,fontWeight: '500',marginLeft:8}}> {subcat.name} </Text>
                 </TouchableOpacity>
               </View>
           )}
           <CatProductList navigation={this.props.navigation} cat={data.name}/>
          </ScrollView>
          :
         <Text style={{color:color}}>
           No Sub Categories available
         </Text> 
        :
          <Loader />
        }
      </View>
    );
  }
}
