import React, { Component } from 'react';
import { View, Text,StyleSheet,TouchableOpacity,Image } from 'react-native';

export default class CatCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
      const data = this.props.data
    return (
      <View style={styels.container}>
         <TouchableOpacity activeOpacity={0.8} onPress={()=>this.props.navigation.navigate('Category',{name:data.name,data})} 
         style={{flex:1}}>
            <Image source={data.img ? {uri : data.img} : require('../../../assets/images/photo.png')} style={{flex:3,alignItems: 'stretch',}}/>
            <Text style={{textAlign:'center',alignItems: 'center',fontWeight: '400',fontSize:12}}> {data.name} </Text>
          </TouchableOpacity>
      </View>
    );
  }
}

const styels = StyleSheet.create({
    container:{height:90,width:130,backgroundColor: '#fff', marginHorizontal: 5,borderWidth:1,borderColor:'#ddd'}
})
