import React, { Component } from 'react';
import { View, Text,flatList ,ScrollView } from 'react-native';
import {strings,Loader} from '../assets'
import ArrivalSlider from './ArrivalSlider'
import getdata from '../functions/async'
import Snackbar from 'react-native-snackbar'
import firebase from 'react-native-firebase'

const db = firebase.firestore()
export default class NewArrivals extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list : []
    };
  }
  componentDidMount = () => {
     getdata('uid').then(uid=>{
       this.setState({uid})
       //alert(uid)
       this.unsubscribe = db.collection('wishlists').doc(uid)
                          .onSnapshot(res=>{
                            const list = res.exists ? res.data().list : []
                          
                            this.setState({list})
                          })
     }).catch(err=>console.log(err))
  

    }
  
  wishList = (id) =>{
     
      const {list,uid} = this.state
      const bool = !list.includes(id)
       bool ? list.push(id) : list.splice(list.indexOf(id),1)
        this.setState({list})
      db.collection('wishlists').doc(uid).set({list,uid})
      .then(res =>{
          const msg = bool ? 'One Item Is Added To the WishList' : 'One Item Removed From Wishlist'
          Snackbar.show({
            title: `${msg}`,
            duration: Snackbar.LENGTH_SHORT
          })
      }).catch(err=>console.log(err))
      
     //sync with the database here,

  }
  componentWillUnmount = () => {
    this.unsubscribe()
  }
  
  
  

  render() {
      const docs = this.props.docs || []
      const {list} = this.state
    return (
      <View>
        <Text style={{textAlign:'center',color:'#000',fontSize:18,fontWeight: '500',margin:8}}> NEW ARRIVALS </Text>
       { docs.map((data,id)=>
            <View key={id} style={{backgroundColor: '#FFF',marginVertical:5}}>
              <Text style={{margin:5,fontWeight: '400',}}> {data.data().name.toUpperCase()} </Text>
               <ArrivalSlider navigation={this.props.navigation} data={data} list={list} wishList={this.wishList} />
            </View>
        )}
      </View>
    );
  }
}
