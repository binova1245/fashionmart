import React, { Component } from 'react';
import { View, Text,ScrollView,Image } from 'react-native';
import firebase from 'react-native-firebase'
import {strings} from '../assets'
import ArrivalCard from './ArrivalCard'

const {color,pink,grey,green} = strings
const db = firebase.firestore()
export default class ArrivalSlider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      list:[]
    };
  }
  componentDidMount = () => {
    const data = this.props.data
    const category = data.data().name
    db.collection('products')
    .where('category',"==",category)
    .orderBy('uploadTime',"DESC")
    .limit(5).get()
    .then(res=>{
        const docs = res.size ? res.docs : null
        this.setState({docs,fetched:true})
    })
  }

  

  render() {
      const {docs,fetched,} = this.state
      const {list,wishList} = this.props
      const slider = [1,2]
    return (
      <View>
          {fetched ? docs ?
              <ScrollView horizontal={true} showsHorizontalScrollIndicator={false} style={{marginVertical:5,}}>
                {docs.map((data,id)=>
                   <ArrivalCard data={data} navigation={this.props.navigation} list={list} wishList={wishList} id={data.id} key={id}/>
                    )}
             </ScrollView>
             :
             <View style={{height:250,margin:5,justifyContent: 'center',}}>
                <Text style={{fontSize:16,fontWeight: '500',textAlign:'center'}}>
                   There Are No Products In This Category
                </Text>
             </View>
             : <View style={{flexDirection:'row',justifyContent:'space-around',backgroundColor: '#fff',}}>
                  {slider.map((data,id)=>
                    <ArrivalCard key={id}  />
                     )}
               </View>
             }  
       
      </View>
    );
  }
}
