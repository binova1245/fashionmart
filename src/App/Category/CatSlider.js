import React, { Component } from 'react';
import { View, Text ,StyleSheet ,ScrollView } from 'react-native';
import {strings,Loader} from '../assets';
import firebase from 'react-native-firebase'
import {Spinner} from 'native-base'
import CatCard from './CatCard'
import Snackbar from 'react-native-snackbar'
import NewArrivals from './NewArrivals'

const db  = firebase.firestore()
export default class CatSlider extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  getCategory = () =>{
    this.unsubscribe = db.collection('category')
                       .where("active","==",true)
                       .onSnapshot(res=>{
                         const docs = res.size ? res.docs : null
                         this.setState({docs,fetched:true})
                       })
  }
  
 componentDidMount = () => {
   this.getCategory()
  //  Snackbar.show({
  //    title: 'Product Selected Successfully',
  //    duration: Snackbar.LENGTH_SHORT,
  //  });
 }

 componentWillUnmount = () => {
   this.unsubscribe()
 };
 
 

  render() {
    const st = this.state
    
    return (
      <View style={{flex:1}}>
      <View style={styles.container}>
      
       {st.fetched?
        st.docs ?
         <ScrollView style={{flex:1}} horizontal={true} showsHorizontalScrollIndicator={false} >
           {st.docs.map((data,id)=>
             <CatCard navigation={this.props.navigation} data={data.data()} key={id} />
            )}
        </ScrollView> : 
           <Text style={{textAlign:'center',alignItems: 'center',color:'white'}}>
           Sorry, No Categories Are Available</Text>
          : <Loader/>
           }
     
      </View>
        {st.fetched ?  <NewArrivals navigation={this.props.navigation} docs={st.docs}/> : <Loader/> }
      </View>
    );
  }
}

const styles = StyleSheet.create({
   container:{height:110,elevation:6,paddingVertical: 10,backgroundColor: strings.color}
})