import React, { Component } from 'react';
import { View, Text , Image,TouchableOpacity  } from 'react-native';
import {strings} from '../assets'
import {Icon} from 'native-base'

const {silver,pink,color,grey,green} = strings
export default class ArrivalCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }
 
  wishList = (pid)=>{
   //add pid to the wish list
   const wishList = !this.state.wishList
   this.props.wishList(pid,wishList)
  //  this.setState({wishList})
  }
  componentDidMount = () => {
    console.log(this.props)
  }
  
  render() {
     const data = this.props.data
     const data1 = data ?  data.data()  : {}
     const pid = this.props.id
     const {list} = this.props
    return (
       <View style={{width:175,height:250,margin:8,backgroundColor: strings.silver,borderWidth: 1,borderColor:silver}}>
          <TouchableOpacity onPress={()=> pid ? this.props.navigation.navigate('SingleProduct',{data,name:data1.name}) : null} 
           style={{flex:1}} activeOpacity={0.9}>
             <Image  style={{alignSelf: 'stretch',flex:1}} source={{uri :data ? data.data().img[0] : null}}/>
           <View style={{backgroundColor: '#fff',height:40,position:'absolute',width:173,bottom:0,left:0}}/>  
           <View style={{backgroundColor: '#fff',alignSelf: 'center',height:40,width:150,bottom:20}}>
             <Text style={{textAlign:'center',fontWeight: '500',marginTop:3,fontSize:12,color:color}}> {data1.name} </Text>
             <Text style={{textAlign:'center',fontWeight: '500',marginTop:3,fontSize:14,color:green}}>
              {data1.Piece ? ' ₹ ' + data1.Piece.price : data1.subcat } </Text>
           </View>

           <Icon name="heart" type="FontAwesome" onPress={() => this.props.wishList(pid)}
                   style={{fontSize:22,position:'absolute',left:8,bottom:5,
                   color:list ? list.includes(pid)? strings.pink : grey : grey,}}/>
               
          </TouchableOpacity>
      </View>
    );
  }
}
