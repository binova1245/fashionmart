import React, { Component } from 'react';
import { View, Text, StatusBar } from 'react-native';
import { createSwitchNavigator } from 'react-navigation'
import { Icon } from 'native-base'
import Drawer from './src/App/dashboard/index'
import Login from './src/App/Auth/Login'
import firebase from 'react-native-firebase';
import {setdata} from './src/App/functions/async'


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggedIn: false
    };
  }

  componentDidMount() {
    //firebase.auth().signOut();
    this.unsubscribe = firebase.auth().onAuthStateChanged((user) => {
    
      setTimeout(() => this.props.navigation.navigate(!user ? 'login' : 'App', { user }), 100)
    });
  };

  componentWillUnmount() {
    if (this.unsubscribe) this.unsubscribe();
  }


  render() {
    return (
      <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
        <StatusBar translucent={true} backgroundColor='#fff' />
       <Text style={{fontSize:30,textAlign:'center',color:'teal'}}>FashionMart</Text>
      </View>
    );
  }
}

//Create a switch for authenticated users

const Switch = createSwitchNavigator({
  Home: App,
  login: Login,
  App: Drawer
})

export default Switch;
